---
id: v4.1.6
title: v4.1.6 (2020-05-10)
---

# v4.1.6 (2020-05-10)

## 修复
- [Issue 36](https://github.com/zycgit/hasor/issues/36) oracle 分页模式下 select count 语句无法正确执行。
