---
id: v4.1.10
title: v4.1.10 (2020-07-06)
---

# v4.1.10 (2020-07-06)

## 新增
- [Issue I1IZ16](https://gitee.com/zycgit/hasor/issues/I1IZ16) 增加 LookupDataSourceListener 扩展接口，允许每次执行 DataQL 的 SQL 片段时都动态的查找 DataSource 以实现动态数据源。
- [Issue I1M4FY](https://gitee.com/zycgit/hasor/issues/I1M4FY) CollectionUdfSource 函数库需要一个 size 函数
- collect 函数库新增 size、newMap、groupBy 三个函数。

## 优化
- 函数库调整：已有 hex 函数库合入 convert 函数库；已有 compare 函数库合入 string 函数库；已有 hmac 函数库合入 codec 函数库；
- AppContext 接口的 joinSignal 方法废弃。增加一个 waitSignal 方法（可以利用对象锁的形式进行等待）
- [Issue I1KFAZ](https://gitee.com/zycgit/hasor/issues/I1KFAZ) 优化 DomainHelper 增加对 UUID 类型的默认支持，将其转换为 String

## 修复
- [Issue I1M4CH](https://gitee.com/zycgit/hasor/issues/I1M4CH) 修复：带有 content-path 的工程生成的 Swagger 文档中 BaseURL少了一级路径
- [Issue I1M4FS](https://gitee.com/zycgit/hasor/issues/I1M4FS) 修复：ResultStructure 取消勾选之后，在刷新又显示被勾选了
- [Issue I1MM4L](https://gitee.com/zycgit/hasor/issues/I1MM4L) 修复：4.1.9版本获取header参数Bug
- [Issue I1MJV1](https://gitee.com/zycgit/hasor/issues/I1MJV1) 修复：自定义序列化返回，首页未展示结果，编辑页能展示结果并下载
