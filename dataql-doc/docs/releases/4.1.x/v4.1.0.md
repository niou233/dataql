---
id: v4.1.0
title: v4.1.0 (2020-02-03)
---

# v4.1.0 (2020-02-03)

## 新增
- DataQL 中的 InterBeanMap 更名为 BeanMap 移到 commons 中。

## 优化
- 重构，放弃 javacc 更换成 antlr4。antlr4 更加智能。AST 模型仍然不变。重构后单测覆盖率达到 90%。
- DataQL 大量新语法新特性。具体参看语法参考手册。一些老的语法形式也不在支持，因此 DataQL 的语法和以前有明显变化。
- 运行时内存模型：确定为 两栈一堆。
- 指令集系统：不在需要 ASM、ASA、ASO 三个指令，取而代之的是更严谨的指令集。
- SDK：函数包能力
- DataModel数据模型：增加 unwrap 方法，用来解开 DataModel 包裹
- 新增 Fragment 机制允许 DataQL 执行外部非 DataQL 语法的代码片段。
- BeanContainer 改为 Finder，删掉 UdfSource、UdfManager、UdfResult 不在需要这些概念。
- 原有 dql test case 语句文件统一转移到 _old 目录下面备用。
