---
id: v3.2.2
title: v3.2.2 (2018-01-02)
---

# v3.2.2 (2018-01-02)

## 新增
- DataQL UDF 新增 manager 接口方便管理和注册。
- DataQL 新增了 import 语法，现在可以导入另外一个 QL 查询作为 udf 导入到当前 QL 中了。

## 改进
- DataQL 的 LoaderUdfSource 增加 isIgnore 方法用于判断是否忽略不正确的UDF查找请求
- DataQL 优化udf source增加机制。
